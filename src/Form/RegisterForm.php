<?php

namespace Drupal\register_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Class RegisterForm.
 *
 * @package Drupal\register_form\Form
 */
class RegisterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'register_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['name']       = array(
      '#type'          => 'textfield',
      '#title'         => $this->t('Enter your name'),
      '#default_value' => $form_state->getValue('name') ? $form_state->getValue('name') : '',
      '#required'      => TRUE,
      '#ajax'          => [
        'callback' => '::validateNameAjax',
        'event'    => 'change',
        'progress' => [
          'type'    => 'throbber',
          'message' => t('Verifying name...'),
        ],
      ],
      '#suffix'        => '<div class="name-validate"></div>',
    );
    $form['user_email'] = array(
      '#type'          => 'email',
      '#title'         => $this->t('Enter your email'),
      '#default_value' => $form_state->getValue('user_email') ? $form_state->getValue('user_email') : '',
      '#required'      => TRUE,
      '#ajax'          => [
        'callback' => '::validateEmailAjax',
        'event'    => 'change',
        'progress' => [
          'type'    => 'throbber',
          'message' => t('Verifying email...'),
        ],
      ],
      '#suffix'        => '<div class="email-validate"></div>',
    );
    $form['gender']     = array(
      '#type'          => 'radios',
      '#title'         => $this->t('Gender'),
      '#default_value' => 'male',
      '#options'       => array(
        'male'   => $this->t('M'),
        'female' => $this->t('W'),
      ),
    );
    $form['phone']      = array(
      '#type'          => 'textfield',
      '#title'         => $this->t('Phone'),
      '#default_value' => $form_state->getValue('phone') ? $form_state->getValue('phone') : '',
      '#required'      => TRUE,
      '#pattern'       => '^\d{8,14}',
      '#ajax'          => [
        'callback' => '::validatePhoneAjax',
        'event'    => 'change',
        'progress' => [
          'type'    => 'throbber',
          'message' => t('Verifying phone...'),
        ],
      ],
      '#suffix'        => '<div class="phone-validate"></div>',
    );
    $form['patient']    = array(
      '#type'          => 'radios',
      '#title'         => $this->t('Patient'),
      '#default_value' => 0,
      '#options'       => array(
        'patient' => $this->t('patient'),
        'clinic'  => $this->t('clinic'),
      ),
    );
    $form['country']    = array(
      '#type'          => 'select',
      '#title_display' => 'invisible',
      '#title'         => t('Country'),
      '#options'       => array(
        'USA'       => t('USA'),
        'Ukraine'   => t('Ukraine'),
        'Germany'   => t('Germany'),
        'Australia' => t('Australia'),
        'Canada'    => t('Canada'),
        'Mexico'    => t('Mexico'),
        'UK'        => t('UK'),
      ),
      '#default_value' => $form_state->getValue('country') ? $form_state->getValue('country') : '',
    );

    $form['actions']['#type']  = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => t('Send'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Validate $form['name'].
   */
  public function validateNameAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if (mb_strlen($form_state->getValue('name'), 'utf-8') <= 3) {
      $response->addCommand(new HtmlCommand('.name-validate', t('Name is too short.')));
    }
    else {
      $response->addCommand(new HtmlCommand('.name-validate', ''));
    }
    return $response;
  }

  /**
   * Validate $form['user_email'].
   */
  public function validateEmailAjax(array &$form, FormStateInterface $form_state) {
    $response       = new AjaxResponse();
    $email          = trim($form_state->getValue('email'));
    $emailValidator = \Drupal::service('email.validator')->isValid($email);

    if (!$emailValidator) {
      $response->addCommand(new HtmlCommand('.email-validate', t('Email not correct.')));
    }
    else {
      $response->addCommand(new HtmlCommand('.email-validate', ''));
    }
    return $response;
  }

  /**
   * Validate $form['phone'].
   */
  public function validatePhoneAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $phone    = $form_state->getValue('phone');
    $pattern  = '/^\d{8,14}/i';

    if (!preg_match($pattern, $phone)) {
      $response->addCommand(new HtmlCommand('.phone-validate', t('Telephone is not correct.')));
    }
    else {
      $response->addCommand(new HtmlCommand('.phone-validate', ''));
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values  = $form_state->getValues();
    $name    = $values['name'];
    $email   = $values['user_email'];
    $gender  = $values['gender'];
    $patient = $values['patient'];
    $phone   = $values['phone'];
    $country = $values['country'];

    $db    = \Drupal::database();
    $query = $db->insert('register_form');
    $query->fields(array(
      'name' => $name,
      'email' => $email,
      'gender' => $gender,
      'patient' => $patient,
      'country' => $country,
      'phone' => $phone,
    ));

    $query->execute();

    drupal_set_message(t('User has been submitted'));
    $form_state->setRedirect('<front>');
  }

}
