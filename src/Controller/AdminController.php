<?php

namespace Drupal\register_form\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Returns responses for user routes.
 */
class AdminController extends ControllerBase {

  /**
   * Displays content links for available users.
   *
   * @return array
   *   A render array for a list of the users that can be editing or deleting.
   */
  public function userListBuilder() {

    $header = [
      ['data' => $this->t('Name'), 'field' => 'name', 'sort' => 'asc'],
      ['data' => $this->t('email')],
      ['data' => $this->t('Name')],
      ['data' => $this->t('Phone')],
      ['data' => $this->t('Country')],
      ['data' => $this->t('Gender')],
      ['data' => $this->t('Patient')],
      ['data' => $this->t('Actions')],
    ];

    $db    = \Drupal::database();
    $query = $db->select('register_form', 'r');
    $query->fields('r', [
      'id',
      'email',
      'name',
      'country',
      'phone',
      'gender',
      'patient',
    ]);

    if ($header) {
      $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
                          ->orderByHeader($header);
      $pager      = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender');
      $result     = $pager->execute()->fetchAll();;
    }
    else {
      $result = $query->execute()->fetchAll();;
    }


    $rows = array();
    foreach ($result as $row => $content) {

      $name    = $content->name;
      $email   = $content->email;
      $phone   = $content->phone;
      $country = $content->country;
      $patient = $content->patient;
      $gender  = $content->gender;

      $path      = '/admin/config/content/bd_contact/edit/' . $content->id;
      $edit      = Url::fromUri('internal:' . $path);
      $edit_link = Link::fromTextAndUrl(t('Edit'), $edit)->toString();

      $path     = '/admin/config/content/bd_contact/delete/' . $content->id;
      $del      = Url::fromUri('internal:' . $path);
      $del_link = Link::fromTextAndUrl(t('Delete'), $del)->toString();

      $mainLink = $this->t('@edit | @delete', [
        '@edit'   => $edit_link,
        '@delete' => $del_link,
      ]);

      $rows[] = [
        ['data' => $name],
        ['data' => $email],
        ['data' => $phone],
        ['data' => $country],
        ['data' => $patient],
        ['data' => $gender],
        ['data' => $mainLink],
      ];
    }

    $build = [
      '#markup' => $this->t('User List'),
    ];

    $build['config_table'] = [
      '#theme'  => 'table',
      '#header' => $header,
      '#rows'   => $rows,
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

}
